Privatemail is available at the chrome store now. Link below.
https://chrome.google.com/webstore/detail/privatemail-for-gmail/jpeianbfgilgkmebghcnebbmffopkjko


Intructions to load the PrivateMail for Gmail Chrome Extension from your personal computer

1.  Open Chrome and navigate to settings->extensions
2.  At the top right corner of the page, click the box for "Developer Mode" if it is not checked
3.  Click the button "Load unpacked extension" located at the top of the "Extensions" section
4.  Navigate to the directory containing the source code and click select
5.  The extension is now loaded

***Since this is not a stable version from the chrome web-store, the extension ID may be different.  If this is the case, you will need to generate a client-id and API key for the extension to use the gmail API, then you will need to replace the client id and API key in js/background.js***

6. In the settings->extensions page, find PrivateMail and copy the ID
7. Put this ClientID in line 1 of chrome/js/background.js and save
8. Close and reopen Chrome or reload it through the extensions page