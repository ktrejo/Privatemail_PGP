var app = angular.module('settings-app', ['ui.bootstrap']).controller('settingsCtrl', ['$scope', '$interval', '$timeout', '$uibModal', function($scope, $interval, $timeout, $uibModal) {
	$scope.state = 'setup'
	//accountData object allows ng-include to access
	$scope.accountData = {};
	$scope.accountData.email = null;
	$scope.buttonText = "Set up PrivateMail";
	$scope.buttonDisable = false;
	$scope.alert = false;
	$scope.newSetup = true;

	//ask the background for a random password
	$scope.generatePassword = function() {
		chrome.extension.sendMessage({request: "randomPassword"}, function(response) {
			$timeout(function() {
				$scope.accountData.password = response.result;

			}, 0);
		});

	};

	$scope.toggleImport = function() {
		$scope.newSetup = !$scope.newSetup;
	};

	$scope.authorize = function() {
		chrome.extension.sendMessage({request: "authorize"}, $scope.getEmail);
	};

	//generate a keypair
	$scope.setup = function() {
		$scope.buttonText = "Loading...";
		$scope.buttonDisable = true;
		chrome.extension.sendMessage({
			request: "generateKey",
			name: $scope.accountData.userName,
			email: $scope.accountData.email,
			password: $scope.accountData.password
		});
		$scope.state = 'done'
		$timeout($scope.getPublicKey, 5000);
		$timeout($scope.addEnroller, 5000);
	};

	//add the enroller to the user's keyring
	$scope.addEnroller = function() {
		chrome.extension.sendMessage({request: "initContact"});
	};

	//get the email address form the google api
	$scope.getEmail = function() {
		chrome.extension.sendMessage({request: "emailAddress"}, function(response) {
			console.log(response)
			if(response) {
				$timeout(function() {
					$scope.accountData.email = response.result;
					$scope.getPublicKey();
					//console.log($scope.publicKey)
				}, 0);
			} else {
				$timeout($scope.getEmail, 1000);
			}
		});//callback will be invoked somewhere in the future
	};
	$scope.getEmail();

	//gets the user's key if they have one
	$scope.getPublicKey = function() {
		chrome.extension.sendMessage({
			request: "publicKey",
			email: $scope.accountData.email
		}, function(response) {
			$timeout(function() {
				$scope.publicKey = response.result
				if(!$scope.publicKey) {
					$scope.autoFillName();
				}
			}, 0);
		});
	}

	//gets a name from the enrollment email
	$scope.autoFillName = function() {
		console.log('get name')
		chrome.extension.sendMessage({
			request: "autoName"
		}, function(response) {
			$timeout(function() {
				$scope.accountData.userName = response.result;
			})
		})
	};

	//open a modal to manage the contacts list
	$scope.openContactsList = function() {
		chrome.extension.sendMessage({
			request: "getContactsList"
		}, function(response) {
			//console.log(response)
			$timeout(function() {
				console.log(response);
				$scope.contacts = response.result;
				$scope.openContactsListHelp();
				//console.log($scope.contacts)
			}, 0);
			//console.log($scope.publicKey)
			//$scope.$apply()
		});
	};

	//contacts list helper
	$scope.openContactsListHelp = function() {
		var modalInstance = $uibModal.open({
			templateUrl: "../contacts.html",
			controller: "contactListCtrl",
			size: "lg",
			resolve: {
				items: function() {
					var items = {};
					items.contacts = $scope.contacts;
					items.me = $scope.accountData.email;
					return items;
				}
			}
		});
		modalInstance.result.then(function(ret) {

		}, function() {

		});
	};

	//open the regen key modal
	$scope.openRegen = function() {
		var modalInstance = $uibModal.open({
			templateUrl: "../regen.html",
			controller: "regenCtrl",
			size: "lg",
			resolve: {
				items: function() {
					var items = {};
					items.email = $scope.accountData.email;
					return items;
				}
			}
		});
		modalInstance.result.then(function(ret) {
			console.log('regen success');
			$scope.getPublicKey();
			$scope.alert = true;
			//alert("Remember to send your contacts your new public key so they can still send you encrypted emails.")
		}, function() {
			console.log('regen failure');
			$scope.getPublicKey();
		});
	};

	//open the add contact modal
	$scope.openAddNew = function() {
		var modalInstance = $uibModal.open({
			templateUrl: "../newContact.html",
			controller: "newContactCtrl",
			size: "lg",
			resolve: {
				items: function() {
					var items = {};
					items.email = $scope.accountData.email;
					return items;
				}
			}
		});
		modalInstance.result.then(function(ret) {

		}, function(ret) {

		})
	};

	//clear the message your contacts modal
	$scope.dismissAlert = function() {
		$scope.alert = false;
	};

	//$interval($scope.getPublicKey, 60000)

}]).controller('newContactCtrl', ['$scope', '$timeout', '$uibModalInstance', 'items', function($scope, $timeout, $uibModalInstance, items) {

	//manually add an armored key
	$scope.addKey = function() {
		chrome.extension.sendMessage({
			request: "addContact",
			pubKey: $scope.newKey
		})

		$timeout(function() {
			$scope.newKey = null
		}, 0)
	};

	//send an invite manually
	$scope.invite = function() {
		chrome.extension.sendMessage({
			request: "invite",
			email: [$scope.inviteeEmail],
			name: [$scope.inviteeName]
		})
		$timeout(function() {
			$scope.inviteeName = null;
			$scope.inviteeEmail = null;
		}, 0);
	};

	//functions to close the modal
	$scope.ok = function () {
      $uibModalInstance.close({});
    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss({});
    };

}]).controller('contactListCtrl', ['$scope', '$timeout', '$uibModalInstance', 'items', function($scope, $timeout, $uibModalInstance, items) {
	$scope.contactList = items.contacts;
	$scope.wrap = {}
	$scope.wrap.me = items.me

	//load a new version of the contacts list
	$scope.updateContacts = function() {
		$scope.newKey = "";

		chrome.extension.sendMessage({
			request: "getContactsList"
		}, function(response) {
			$timeout(function() {
				console.log(response);
				$scope.contactList = response.result;
			}, 0);
		});
	};

	$scope.addKey = function() {
		chrome.extension.sendMessage({
			request: "addContact",
			pubKey: $scope.newKey
		})
		$timeout($scope.updateContacts, 100);
	};

	$scope.deleteContact = function(contact) {
		chrome.extension.sendMessage({
			request: "removeContact",
			email: contact.email_address
		})
		$timeout($scope.updateContacts, 100);
	};

	$scope.ok = function () {
      $uibModalInstance.close({});
    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss({});
    };
}]).controller('regenCtrl', ['$scope', '$uibModalInstance', '$timeout', 'items', function($scope, $uibModalInstance, $timeout, items) {
	$scope.accountData = {};
	$scope.accountData.email = items.email;
	$scope.buttonText = "Generate New Key";
	$scope.newSetup = true;

	$scope.ok = function () {
		chrome.extension.sendMessage({
			request: "revokeKeyPair",
			name: $scope.accountData.userName,
			email: $scope.accountData.email,
			password: $scope.accountData.password
		});
		$scope.state = 'done'
		$scope.buttonText = "Loading...";
		$scope.buttonDisable = true;
      	$timeout($uibModalInstance.close, 5000)	;
    };

    $scope.generatePassword = function() {
		chrome.extension.sendMessage({request: "randomPassword"}, function(response) {
			console.log(response)
			$timeout(function() {
				$scope.accountData.password = response.result;
			}, 0);
		});
	};

	$scope.toggleImport = function() {
		$scope.newSetup = !$scope.newSetup;
	};

	$scope.getName = function() {
		chrome.extension.sendMessage({request: "name"}, function(response) {
			console.log(response);
			$timeout(function() {
				$scope.accountData.userName = response.result;
			}, 0)
		})
	};
	$scope.getName();


    $scope.cancel = function () {
      $uibModalInstance.dismiss({});
    };
}]);
