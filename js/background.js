var clientId = '';
var apiKey = '';
var scopes = 'https://www.googleapis.com/auth/gmail.settings.basic https://www.googleapis.com/auth/gmail.modify';
var privateMailLabelId = null;
var password = null;
var email = null;
var initInterval = true;
var messageText = null;

// Check whether the extension was just added, if it was open options after gmail is authorized
chrome.runtime.onInstalled.addListener(function(details){
  if(details.reason == "install") {
    //open the options page so the user can install
    gmailNotVerified(false);
    
  } else if(details.reason == "update"){
    //if we need to do something after an update
  }
});

//waits until the user authorizes us to the gmail api
var gmailNotVerified = function(imd) {
  gapi.auth.authorize({
    client_id: clientId,
    scope: scopes,
    immediate: imd
  }, function (authResult) {
    if (authResult && !authResult.error) {
      loadGmailApi(imd);
      return true;
    } else {
      gmailNotVerified(false);
      return false;
    }
  })
};

//return true if you will use sendResponse, false otherwise
chrome.extension.onMessage.addListener( function(request,sender,sendResponse) {
  console.log("Received message: " + request);
  //verify we have gmail api access, if we won't spawn the prompt
  //checkAuth(sendResponse);
  if(request.request === "authorize" ) {
    //open the gmail authorize prompt and the options page
    gmailNotVerified(false);
    //chrome.runtime.openOptionsPage();
    return false;       
  } else if(request.request === "openOptionsPage") {
    //open the gmail authorize prompt and the options page
    console.log("open options")
    chrome.runtime.openOptionsPage();
    return false
  } else if(request.request === "emailAddress") {
    //make a gmail call for the user profile
    if(email) {
      sendResponse({result:email});
      return true;
    }
    getProfile('me', function(resp) {
      email = resp.emailAddress;
      sendResponse({result:resp.emailAddress});
      return true;
    })
  } else if(request.request === "generateKey") {
    //set up a key pair
    password = request.password;
    generateKeyPair(request.name, request.email, request.password);
    return false;
  } else if(request.request === "publicKey") {
    //get the users armored key
    var key = getArmoredPublicKey(request.email);
    sendResponse({result: key});
    return true;
  } else if(request.request === "randomPassword") {
    //generate a random password
    sendResponse({result: generatePassphrase()});
    return true;
  } else if (request.request === "getContactsList") {
    //get a ist of all the users contacts with public keys
    sendResponse({result: getContactList()});
    return true;
  } else if(request.request === "encrypt") {
    //encrypt and sign a message
    //for(i=0; i<request.recipient.length; i++) {
    //  request.recipient[i] = getPublicKey(request.recipient[i]);
    //}
    //console.log(request.recipient);
    //var ret 
    //if(request.recipient.length) {
    //var multiKeys = getPublicKeyList(request.recipient);
    //console.log(multiKeys);
    var key_list = getPublicKeyList(request.recipient);
    if(key_list.error) {
      sendResponse({result:false, data: key_list.data});
      return true;
    }
    var ret = encryptMessageMulti(request.recipient, email, password, request.message);
    //} else {
    //  ret = encryptMessage(request.recipient, email, password, request.message);
    //}
    ret.then(function(value) {
      if(!value.error) {
        sendResponse({result:value.data});
      } else {
        sendResponse({result:false});
      }
    }, function(value) {
      console.log(value);
    });
    return true;
  } else if(request.request === "removeContact") {
    //delete a contact's key from the key ring
    revokePublicKey(request.email);
    return false;
  } else if(request.request === "revokeKeyPair") {
    //delete the user's key pair and generate a new key pair
    password = request.password;
    revokeKeyPair(request.email)
    generateKeyPair(request.name, request.email, request.password);
    return false;
  } else if(request.request === "verifyPassword") {
    //check if the user entered the correct password
    if(password) {
      sendResponse({success: true});
    } else {
      //common code to be used after the email is known
      var verifyPassword = function() {
        checkPassphrase(email, request.passwd).then(function() {
            sendResponse({success:true});
            password = request.passwd;
          }, function() {
            sendResponse({success:false});
          });
      };

      if(!email) {
        getProfile('me', function(resp) {
          email = resp.emailAddress;
          verifyPassword();
        });
      } else {
        verifyPassword();
      }
    }
    return true;
  } else if(request.request === "addContact") {
    //add an armored key to hte key ring
    storePublicKey(null, request.pubKey);
    return false
  } else if(request.request === "invite") {
    //send an enrollment email
    if(email) {
      if(request.email.length) {
        for(i=0; i<request.email.length; i++) {
          //console.log('sent request to ' + request.email[i])
          sendAutoRequestMessage(myName(), email, request.email[i], request.name[i]);
        }
      } else {
        sendAutoRequestMessage(myName(), email, request.email, request.name);
      }
      return false;
    }
    getProfile('me', function(resp) {
      email = resp.emailAddress;
      if(request.email.length) {
        for(i=0; i<request.email.length; i++) {
          sendAutoRequestMessage(myName(), email, request.email[i], request.name[i]);
        }
      } else {
        sendAutoRequestMessage(myName(), email, request.email, request.name);
      }
      return false;
    })
  } else if(request.request === "decrypt") {
    //decrypt an email

    //common decrypt code called after email address is known
    var decryptCommon = function() {
      var ret = decryptMessage(request.sender, email, request.message, password);
      ret.then(function(resp) {
        console.log(resp);
        sendResponse(resp);
      });
      return true;
    };

    if(email) {
      decryptCommon();
    } else {
      getProfile('me', function(resp) {
        email = resp.emailAddress;
        decryptCommon();
      })
    }
    return true;
  } else if(request.request === "autoName") {
    //try to autmatically get the name of the new user from an enrollment email
    listMessagesNoLabel('me', 'PrivateMail Enrollment Request from', function(messageList) {
      getMessage('me', messageList[0].id, function(message) {
        messageText = Base64.decode(message.payload.body.data);
        console.log(messageText);
        var regex = new RegExp('Hello (.*),');
        if(regex.exec(messageText)) {
          sendResponse({result: regex.exec(messageText)[1]})
        } else {
          sendResponse({result: null});
        }
      })
    })
    return true;
  } else if(request.request === "initContact") {
    //get the enrollment email and add the enroller's key to the new user's keyring
    if(messageText) {
      storePublicKey(null, messageText);
      var rec = /at (.*) is trying to/.exec(messageText)
      sendAutoReplyMessage(email, rec[1])
    }
    return false;
  } else if(request.request === "havePassword") {
    //a quick check to see if the user already entered the password
    sendResponse({success: password!==null});
  } else if(request.request === "name") {
    //get the users name from their public key
    sendResponse({result: myName()});
  } else {
    return false;
  }
  return false;
});

//use the user's key to get their name
var myName = function() {
  var pubKey = getPublicKey(email);
  //console.log(pubKey[0].users)
  var currUserId = pubKey[0].users[0].userId.userid;
  //console.log(currUserId);

  var len = currUserId.length;
  var index_of_email = currUserId.indexOf('<'); //gets the index of the start of the email block
    
  var name = currUserId.substring(0, index_of_email - 1);
  console.log(name);
  return name;
};

//checks that the privatemail label and filter exist, creates them if they don't
function setupLabelsAndFilters() {
	console.log('Checking for the PrivateMail label');
	/*gets all labels then calls a function that finds the PrivateMail filter*/
	listLabels('me', findPrivateMailLabel);
}

function findPrivateMailLabel(labels) {
  if(!labels) {
    setupLabelsAndFilters();
    return;
  }
	for(i = 0; i < labels.length; i++) {
		if(labels[i].name === 'PrivateMail') {
			privateMailLabelId = labels[i].id;
      //start the crawler if it has not been started already
      if(initInterval) {
        setInterval(crawlAndProcess, 15000, privateMailLabelId);
        initInterval = false;
      }
			findPrivateMailFilter(labels[i].id)
			return;
		}
	}
  //the label was not find, so we create it then move to the filter
	createLabel('me', 'PrivateMail', 'labelHide', 'hide', function(result) {
		findPrivateMailFilter(result.id)
	});
	return;
}

//finds or creates the privatemail filter
function findPrivateMailFilter(labelId) {
  privateMailLabelId = labelId.id;
  listFilter('me', function(filters) {
    if(filters.filter) {
      for(i=0; i<filters.filter.length; i++) {
        if(filters.filter[i].criteria.subject && filters.filter[i].criteria.subject === 'PrivateMail Enrollment Request') {
          return;
        }
      }
    }

		createFilter('me', {"subject": "PrivateMail Enrollment Request"}, {"addLabelIds": [labelId], "removeLabelIds": ['INBOX', 'UNREAD']}, function(resp) {
			console.log('PrivateMail filter created');
		})
	});
}

//after the gmail api has been loaded, do more setup
function handleClientLoad() {
	gapi.client.setApiKey(apiKey);
	gmailNotVerified(true);
}

//check if gmail api use is authorized, if it isn't, ask the uer for permission
/*function checkAuth(sendResponse) {
	gapi.auth.authorize({
		client_id: clientId,
		scope: scopes,
		immediate: true
	}, function (authResult) {
		//console.log(sendResponse)
		if (authResult && !authResult.error) {
			console.log("success, gmail api use is authorized");
			loadGmailApi();
			return true;
		} else {
			console.log("failure, gmail api use is not authorized");
			//sendWrap( {result:false} );
			handleAuthClick();
			return false;
		}
	})
}

//asks the user for gmail access
function handleAuthClick() {
	gapi.auth.authorize({
		client_id: clientId,
		scope: scopes,
		immediate: false
	}, handleAuthResult);
	return false;
}

//same as check Auth but does not send a response after completing
function handleAuthResult(authResult) {
	if(authResult && !authResult.error) {
		console.log("success, gmail api use is authorized")
		loadGmailApi();
		return true;
	} else {
		console.log("failure, gmail api use is not authorized")
		return false;
	}
}*/

//loads the gmail api, and then makes sure the label and filter exist
function loadGmailApi(imd) {
	gapi.client.load('gmail', 'v1', function() {
		console.log("gmail api has been loaded in the background script");
    if(!imd) {
      setTimeout(chrome.runtime.openOptionsPage, 0);
    }
		setupLabelsAndFilters();
		//makePrivateMailFilter();
	});
}

//when the background has loaded, initialize the openpgp.js to be used
document.addEventListener('DOMContentLoaded', function () {
  openpgp = window.openpgp;
  openpgp.initWorker({ path:'backend/openpgp.worker.js' }) // set the relative web worker path
  openpgp.config.aead_protect = true // activate fast AES-GCM mode (not yet OpenPGP standard)
  openpgp.Keyring(openpgp.storeHandler);
});
