/*css selector which finds the send button if it exists*/
var checkSend = function() {
	if(document.querySelector('[data-tooltip~=Send]')) {
		if(!document.getElementById('PrivateMailEncrypt')) {
			var sendButton = document.querySelector('[data-tooltip~=Send]');

			var newDiv = document.createElement("div"); 
			newDiv.className = 'T-I J-J5-Ji aoO T-I-atl L3 T-I-Zf-aw2';
			newDiv.id = 'PrivateMailEncrypt';
			newDiv.addEventListener("click", encryptCurrentText);

  			var newContent = document.createTextNode("Encrypt"); 
 			newDiv.appendChild(newContent); //add the text node to the newly created div. 

  			// add the newly created element and its content into the DOM 
  			sendButton.parentNode.insertBefore(newDiv, sendButton); 
  		}
		
	}
};

/*css selector which finds the when to add a decrypt */
var checkDecrypt = function() {
	if(document.querySelector('.ade')) {
		if(!document.getElementById('PrivateMailDecrypt')) {
			var decDiv = document.querySelector('.hP');

			var newDiv = document.createElement("div"); 
			newDiv.className = 'T-I J-J5-Ji aoO T-I-atl L3 T-I-Zf-aw2';
			newDiv.id = 'PrivateMailDecrypt';
			newDiv.addEventListener("click", decryptCurrentMessage);

  			var newContent = document.createTextNode("Decrypt"); 
  			decDiv.appendChild(document.createElement("br"))
 			newDiv.appendChild(newContent); //add the text node to the newly created div. 

  			// add the newly created element and its content into the DOM 
  			checkAutoDecrypt();
  			decDiv.appendChild(newDiv); 
  		}
		
	}
};

var encryptCurrentText = function() {
	getPassword(encryptCurrentText2);
};


var encryptCurrentText2 = function() {
	//console.log('encrypt function called')
	document.querySelector('.Am.Al.editable.LW-avf').focus();
	setTimeout(function() {
		var messageBody = document.querySelector('.Am.Al.editable.LW-avf').innerHTML;
		var recipientParent = document.querySelector('.oL.aDm.az9');
		var recipients = document.querySelector("div.oj>div.wO.nr").querySelectorAll("div.vR");
		//console.log(recipients);

		if(recipientParent) {
			//found receiant element
			var recipientArray = [];
			var recipient = recipientParent.firstChild.getAttribute('email')
			for(i=0; i<recipients.length; i++) {
				if(recipients[i].getAttribute) {
					if(recipients[i].firstChild.getAttribute('email')) {
						recipientArray.push(recipients[i].firstChild.getAttribute('email'));
					}
				}
			}
			console.log(recipientArray);

			chrome.extension.sendMessage(
				{
					request: "encrypt",
					message: messageBody,
					recipient: recipientArray	
				}, function(response) {
					if(response.result) {
						document.querySelector('.Am.Al.editable.LW-avf').innerHTML = response.result;
					} else {
						//error encrypting data
						var inviteText = "You do not have the encryption keys for the following people you are trying to email:\n";
						for(i=0; i<response.data.length; i++) {
							inviteText += "    " + response.data[i] + "\n";
						}
						inviteText += 'You can send encrypted emails to the contact(s) above when you receive their encryption key(s) check whose encryption keys you have by going to the options page and clicking "Manage Your Contacts\' Encryption Keys"\n\n';
						inviteText += "Click ok to ask these people to join PrivateMail or share their existing encryption key."
						var inviteeName = confirm(inviteText);
						if (!inviteeName) {
	    				} else {
	    					var nameArray = [];
	    					for(i=0; i<response.data.length; i++) {
	    						var email = document.querySelector('[email="' + response.data[i] + '"]');
	    						//console.log(email);	
	    						var toAdd = />(.*) \(/.exec(email.innerHTML);
	    						if(toAdd) {
	    							nameArray.push(toAdd[1]);
	    						} else {
	    							nameArray.push("");
	    						}
	    					}
	    					//console.log(nameArray);
							chrome.extension.sendMessage({
								request: "invite",
								email: response.data,
								name: nameArray
							}, function(response) {
							})
						}
					}
				});

		} else {
			//no recipiant entered
		}
	}, 100);
};

var checkAutoDecrypt = function() {
	var toDec = document.querySelector('.a3s.aXjCH').innerHTML.replace(/<\/?[^>]+(>|$)/g, "");
	var senderEmail = document.querySelector('.gD').getAttribute('email');

	chrome.extension.sendMessage({
		request: "decrypt",
		sender: senderEmail,
		message: toDec
	}, function(response) {
		if(!response.error){
			//key unlocked
			var replace = document.querySelector('.a3s.aXjCH').innerHTML;
			var value = response.data.decryptedText;

			if(!(response.data.isValidSig)){
				window.alert("WARNING: The identity of the sender cannot be validated");
			}
			
			document.querySelector('.a3s.aXjCH').innerHTML = "<div dir='ltr'>" + value + "</div>";

			var newDiv = document.createElement("span");
			newDiv.innerHTML = " This message has been automatically decrypted";
			newDiv.id = 'WasAutoDecrypted';
			newDiv.className = "hb";
			newDiv.style['background-color'] = "#a2ff6a"

			document.querySelector('.ady').appendChild(newDiv);
		} else {

		}
	})	
};

var decryptCurrentMessage = function() {
	getPassword(decryptCurrentMessage2);
};

var decryptCurrentMessage2 = function() {
	var toDec = document.querySelector('.a3s.aXjCH').innerHTML.replace(/<\/?[^>]+(>|$)/g, "");
	var senderEmail = document.querySelector('.gD').getAttribute('email');

	chrome.extension.sendMessage({
		request: "decrypt",
		sender: senderEmail,
		message: toDec
	}, function(response) {
		if(!response.error){
			//key unlocked
			var replace = document.querySelector('.a3s.aXjCH').innerHTML;
			var value = response.data.decryptedText;

			if(!(response.data.isValidSig)){
				window.alert("WARNING: The identity of the sender cannot be validated");
			}
			document.querySelector('.a3s.aXjCH').innerHTML = "<div dir='ltr'>" + value + "</div>";
		} else {
			alert('Error decrypting message')
		}
	})	
};

setInterval(checkSend, 1000);
setInterval(checkDecrypt, 1000);

var getPassword = function(callback) {
	chrome.extension.sendMessage({
		request: "havePassword"
	}, function(response) {
		if(response.success) {
			callback();
			return;
		}
   		var password = prompt("Please enter your PrivateMail password:", "");
   		if (password == null || password == "") {
   		} else {
    		chrome.extension.sendMessage({
				request: "verifyPassword",
				passwd: password,
			}, function(response) {
				if(response.success){
					//key unlocked
					callback();
				} else {
					getPassword(callback);
				}
			})
    	}
	});
};