var privateMailLabelId = null;
var email = null;

/*gets an array of message ids based on the query*/
function listMessagesNoLabel(userId, query, callback) {
  var getPageOfMessages = function(request, result) {
    request.execute(function(resp) {
    	console.log(resp);
      result = result.concat(resp.messages);
      var nextPageToken = resp.nextPageToken;
      if (nextPageToken) {
        request = gapi.client.gmail.users.messages.list({
          'userId': userId,
          'pageToken': nextPageToken,
          'q': query
        });
        getPageOfMessages(request, result);
      } else {
        callback(result);
      }
    });
  };
  var initialRequest = gapi.client.gmail.users.messages.list({
    'userId': userId,
    'q': query
  });
  getPageOfMessages(initialRequest, []);
}

//The first part of message crawling; get list of messages
function crawlAndProcess(privateMailLabel) {
	privateMailLabelId = privateMailLabel;
	listPrivateMailMessages(crawlAndProcess2);
}

//for each message id in the private mail label, the the message and process it
function crawlAndProcess2(messageArray) {
	for (i=0; i<messageArray.length; i++) {
		if(messageArray[i]) {
			getMessage('me', messageArray[i].id, handleAutoMessage);
		}
	}
}

//helper function that get the messages with teh private mail label
function listPrivateMailMessages(callback) {
	var getPageOfMessages = function(request, result) {
		request.execute(function(resp) {
			console.log(resp);
			result = result.concat(resp.messages);
			var nextPageToken = resp.nextPageToken;
			if (nextPageToken) {
				request = gapi.client.gmail.users.messages.list({
					'userId': 'me',
					'pageToken': nextPageToken,
					'labelIds': privateMailLabelId
				});
				getPageOfMessages(request, result);
			} else {
				callback(result);
			}
		});
	};
	var initialRequest = gapi.client.gmail.users.messages.list({
		'userId': 'me',
		'labelIds': privateMailLabelId
	});
	getPageOfMessages(initialRequest, []);
}

//get the entire message based on id
function getMessage(userId, messageId, callback) {
	console.log(messageId)
	var getMessageRequest = function(request, result){
		request.execute(function(resp){
			callback(resp);
		});
	};

	var initialRequest = gapi.client.gmail.users.messages.get({
		'userId': userId,
    	'id': messageId
	});

	getMessageRequest(initialRequest, []);
}

//user id is the authorized users id
//messageId is the id of the message
//call back is a callback function.
function deleteMessage(userId, messageId, callback){

	var deleteMessageRequest = function(request, result){
		request.execute(function(resp){
			if(callback) {
				callback(resp);
			}
		});
	};

	var initialRequest = gapi.client.gmail.users.messages.trash({
		'userId': userId,
    	'id': messageId
	});

	deleteMessageRequest(initialRequest, []);
}

/**
 * Handle Automated Message.
 *
 * @param  {JSON} email Chrome message json
 */
function handleAutoMessage(message) {
	//delete the message; we will never use it again
	deleteMessage('me', message.id, function(){});

	//find the subject header, and respond/react based on what type of message it is
	for (i=0; i<message.payload.headers.length; i++) {
		if (message.payload.headers[i].name === "subject" || message.payload.headers[i].name === "Subject") {
			//32nd character of key requests is 'f' in "from"
			if (message.payload.headers[i].value[31] === 'f') {
				console.log('Crawler found key request');
				handleAutoRequestMessage(message);
			//32nd character of key replies is 'k' in "key"
			} else if (message.payload.headers[i].value[31] === 'k') {
				console.log("Crawler found key reply");
				handleAutoReplyMessage(message);
			}
		}
	}
}

/**
 * Handle Automated Key Request Message.
 *
 * @param  {JSON} email Chrome message json
 * @param  {Function} callback Function to call when the request is complete.
 */
 function handleAutoRequestMessage(message) {
 	//we need to send the sender the users key
 	for (i=0; i<message.payload.headers.length; i++) {
		if (message.payload.headers[i].name === "from" || message.payload.headers[i].name === "From") {
			var sender = message.payload.headers[i].value;
		}
	}
	var messageText = Base64.decode(message.payload.body.data);
	storePublicKey(null, messageText);
	if (!email) {
		getProfile('me', function(resp) {
		email = resp.emailAddress;
		sendAutoReplyMessage(email, sender);
		})
	} else {
		sendAutoReplyMessage(email, sender);
	}
}

/**
 * Handle Automated Key reply Message.
 *
 * @param  {JSON} email Chrome message json
 */
function handleAutoReplyMessage(message) {
	var armoredKey = Base64.decode(message.payload.body.data);
	storePublicKey(null, armoredKey);
}

/**
 * Send Public Key Reply Message.
 *
 * @param  {String} sender email address of sender
 * @param  {String} recipient email address of recipient
 */
function sendAutoReplyMessage(sender, recipient) {
	var sendTime = Date().toString();
	var formattedSendTime = sendTime.slice(0,2) + "," + sendTime.slice(7,9) + 
			sendTime.slice(3,6) + sendTime.slice(10,24) + sendTime.slice(27,31);
	var key = getArmoredPublicKey(sender);

	var message = "from: " + sender + "\r\norig-date: " + formattedSendTime + "\r\nto: " + 
		recipient + "\r\nsubject:PrivateMail Enrollment Request key reply\r\n\r\n" + 
		key + "\r\n";

	sendAutoMessage(message);
}

/**
 * Send Enrollment Request Message.
 *
 * @param  {String} senderName name of sender 
 * @param  {String} senderEmail email address of sender
 * @param  {String} email address of recipient
 * @param  {String} recipient's name
 */
function sendAutoRequestMessage(senderName, senderEmail, recipient, recipientName) {
	var sendTime = Date().toString();
	var formattedSendTime = sendTime.slice(0,2) + "," + sendTime.slice(7,9) + 
			sendTime.slice(3,6) + sendTime.slice(10,24) + sendTime.slice(27,31);

	var key = getArmoredPublicKey(senderEmail);
	var message = "from: " + senderEmail + "\r\norig-date: " + formattedSendTime + "\r\nto: " + recipient + 
		"\r\nsubject:PrivateMail Enrollment Request from " + senderName + "\r\n\r\nHello";
		if (recipientName !== "") {
			message += " ";
		}
		message += recipientName + ",\r\n\r\nYour friend " + senderName + " at " + senderEmail + 
		" is trying to send you a secure email with PGP encryption. But you'll need to download PrivateMail for them to do " + 
		"that!\r\n\r\nPrivateMail is an extension for Gmail on Google Chrome that handles " + 
		"email encryption and decryption for you. This means you can send and receive emails in state of the art privacy, " + 
		"without even having to lift a finger. All you have to do is download it.\r\n\r\nYou can get PrivateMail " + 
		"right now for free with this link:\r\n\r\nPrivateMail for Google Chrome: " + 
		"https://chrome.google.com/webstore/detail/privatemail-for-gmail/jpeianbfgilgkmebghcnebbmffopkjko\r\n\r\n" + 
		"If you already have a PGP key, please send your key to me so I can send my encrypted message. " + 
		"You can also add my key to your key manager:\r\n\r\n" + key + "\r\n";

	console.log(message);

	sendAutoMessage(message);
}

/**
 * Send Automated Message.
 *
 * @param  {String} email full RFC 5322 compliant email
 */
function sendAutoMessage(message) {
	//send raw email; base64 encode
	var base64EncodedEmail = Base64.encodeURI(message);
	var request = gapi.client.gmail.users.messages.send({
		'userId': 'me',
		'resource': {
			'raw': base64EncodedEmail
		}
	});
	request.execute(function(resp) {
		//delete the message so we don't process it
		deleteMessage('me', resp.id);
	});
}

//user id is where the email is sent
//email is the mime message
//call back is a callback function.
//FOR THIS FUNCTION, USERID SHOULD ALWAYS BE 'me'
function sendMessage(userId = 'me', email, callBack){

    var base64EncodedEmail = Base64.encodeURI(email);    
	var sendMessageRequest = function(request, result){

		request.execute(function(resp){
			
			callBack(resp);
		});
	};
	var initialRequest = gapi.client.gmail.users.messages.send({
		'userId': userId,
		'resource': {
      			'raw': base64EncodedEmail
    	}
	});
 
	sendMessageRequest(initialRequest, []);	
}

//----------------------------------------------------------------------------
//-------------------------------label functions-------------------------------
//----------------------------------------------------------------------------
//list all labels of userId
function listLabels(userId, callback){

	var listLabelsRequest = function(request, result){

	  request.execute(function(resp) {
	    var labels = resp.labels;
	    callback(labels);
	  });
	};

	var initialRequest = gapi.client.gmail.users.labels.list({
	    'userId': userId
	});
	listLabelsRequest(initialRequest,[]);
}

function createLabel(userId, labelName, labelListVisibility='labelShow', messageListVisibility='show', callBack){
	var createLabelRequest = function(request){
		request.execute(function(resp) {
	    callBack(resp);
	  });
	};
	var initialRequest = gapi.client.gmail.users.labels.create({
	    'userId': userId,
	    'labelListVisibility'   : labelListVisibility,
	    'messageListVisibility' : messageListVisibility,
	    //aparently the label fields don't get their own object even though thats what the api says to do
	    'name': labelName
  	});
	createLabelRequest(initialRequest);
}
//labelId is the id of the label to retrieve.
function getLabel(userId, labelId, callBack){

	var getLabelRequest = function(request, result){
	 
  		request.execute(function(resp){			
			callBack(resp);
		});
	};

	var initialRequest = gapi.client.gmail.users.labels.get({
	    'userId': userId,
	    'id': labelId
  	});
  	getLabelRequest(initialRequest,[]);
}

//my own custom function.
//labelsToAdd is an array of label ids
//labelsToRemove is an array of Label ids
//message id is the message you want to manipulate the labels on.
function modifyMessageLabels(userId, messageId, labelsToAdd, labelsToRemove, callback){
	 var request = gapi.client.gmail.users.messages.modify({
    'userId': userId,
    'id': messageId,
    'addLabelIds': labelsToAdd,
    'removeLabelIds': labelsToRemove
  });
  request.execute(callback);
}
//----------------------------------------------------------------------------
//-------------------------------filter functions-------------------------------
//----------------------------------------------------------------------------
//criteria is the critera in the created filter
//https://developers.google.com/gmail/api/v1/reference/users/settings/filters#resource for info on criteria
//criteria is the criteria oject of the filter
//action is an action object of the filter
function createFilter(userId, criteria, action, callBack){
  var loopRequest = function(request){
  	request.execute(function(resp){
  		callBack(resp);
  	});
  }
  var initialRequest = gapi.client.gmail.users.settings.filters.create({
    'userId': userId,
    'criteria': criteria,
    'action': action
  });
  loopRequest(initialRequest);
}

function listFilter(userId = 'me', callback) {
	var request = gapi.client.gmail.users.settings.filters.list({
		'userId': userId
	})
	request.execute(callback);
}

function getFilter(userId = 'me', filterId, callback) {
	var request = gapi.client.gmail.users.settings.filters.get({
		'userId': userId,
		'id': filterId
	})

	request.execute(callback);
}

//----------------------------------------------------------------------------
//-------------------------------misc functions-------------------------------
//----------------------------------------------------------------------------
//gets the profile of userId
function getProfile(userId, callback){
	 var request = gapi.client.gmail.users.getProfile({
	   'userId': userId
	 });
	 request.execute(function(resp) {
	   callback(resp);
	 });
}