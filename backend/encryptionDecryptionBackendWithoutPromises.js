//Note to self:Remember to garbage collect encryptedText eventually for final version; the var is only needed for testing
function generateKeyPair(name, email_address, passphrase){
	var options = {
    userIds: [{ name: name, email:email_address }], // multiple user IDs possible supposedly, not sure what that would result in
    numBits: 2048,        // RSA key size
    passphrase: passphrase         // protects the private key
	//keyExpirationTime: 90 //key expiration time, this may need to change
};
openpgp.generateKey(options).then(function(key){
	privkey = key.privateKeyArmored; //armored private key
	pubkey = key.publicKeyArmored; //armored public key
	o = openpgp.storeHandler.loadPublic();//gets an array of unarmored public keys
	o.push(openpgp.key.readArmored(pubkey).keys[0]); //appends the generated public key to the list of unarmored public keys
	openpgp.storeHandler.storePublic(o); //stores the new array of public keys to the store handler, eg: adds the key to the key ring
	p = openpgp.storeHandler.loadPrivate();
	p.push(openpgp.key.readArmored(privkey).keys[0]);
	openpgp.storeHandler.storePrivate(p);
	openpgp.publicKeys.importKey(pubkey); //adds the new armored keys to openpgp.publicKeys and openpgp.privateKeys
	openpgp.privateKeys.importKey(privkey);
	
	pubkey = undefined; //garbage collection
	privkey = undefined;
	o = undefined;
	p = undefined;
	
}).catch(function(e){
	console.log("Caught: " + e.message);
	window.alert(e.message);
	return {error: true, reason: e.message};
}); 
}




function generateKeyPair2(){
/*
Input: (from the document atm): passphrase (from textbox id="PassphraseGen" atm), generators email (atm from textbox id="myEmail"),
generators name (atm from textbox id="myName")

Output: New public private key pair stored in the keyring.

NOTE: This functione exists only for testing purposes.  It will have no impact on the final product.
*/	

myGenerationPassphrase = document.getElementById("PassphraseGen").value;
myEmail = document.getElementById("myEmail").value;
myName = document.getElementById("myName").value;
generateKeyPair(myName, myEmail, myGenerationPassphrase);
myName = undefined;
myEmail = undefined;
myGenerationPassphrase = undefined;
/*var options = {
    userIds: [{ name: myName, email:myEmail }], // multiple user IDs possible supposedly, not sure what that would result in
    numBits: 2048,        // RSA key size
    passphrase: myGenerationPassphrase,         // protects the private key
	keyExpirationTime: 999999 //key expiration time, this may need to change
};
openpgp.generateKey(options).then(function(key){
	privkey = key.privateKeyArmored; //armored private key
	pubkey = key.publicKeyArmored; //armored public key
	o = openpgp.storeHandler.loadPublic();//gets an array of unarmored public keys
	o.push(openpgp.key.readArmored(pubkey).keys[0]); //appends the generated public key to the list of unarmored public keys
	openpgp.storeHandler.storePublic(o); //stores the new array of public keys to the store handler, eg: adds the key to the key ring
	p = openpgp.storeHandler.loadPrivate();
	p.push(openpgp.key.readArmored(privkey).keys[0]);
	openpgp.storeHandler.storePrivate(p);
	openpgp.publicKeys.importKey(pubkey); //adds the new armored keys to openpgp.publicKeys and openpgp.privateKeys
	openpgp.privateKeys.importKey(privkey);
	pubkey = undefined; //garbage collection
	privkey = undefined;
	o = undefined;
	p = undefined;
	
}); 
*/
}


function onLoad(){
	requirejs(['openpgp'], function(openpgp){

//openpgp.initWorker({ path:'openpgp.worker.js' });
//openpgp.initWorker({ path:'openpgpjs-master/dist/openpgp.worker.js' });
//openpgp.config.aead_protect = true;
//openpgp.initWorker({ path:'openpgpjs-master/dist/openpgp.worker.js' });
openpgp.config.aead_protect = true;
	});
}



function decryptPrivateKey(privKey, passphrase){
	/*This function decrypts the given private key using the given passphrase
	*/
	var options = {
		privateKey: privKey,
		passphrase: passphrase
	};
	privKey = undefined; //garbage collection
	passpharase = undefined; //garbage collection
	//privKey = undefined;
	//passphrase = undefined;
	
	privkeyPromise = openpgp.decryptKey(options); //returns the promise
	
	console.log("203");
	
	console.log("204");
	options = undefined;
	return privkeyPromise;
}

function decryptPrivateKey2(){
	//Input (from document atm): Index of private key on disk, and a passphrase
	//Output: A promise that decrypts the private key
	var privkey = openpgp.storeHandler.loadPrivate()[parseInt(document.getElementById("privateKeyIndex").value)]; //private key
	var myPassphrase = document.getElementById("PassphraseDec").value; //passphrase 
	var options = {
		privateKey: privkey,
		passphrase: myPassphrase
	};
	myPassphrase = undefined;
	return openpgp.decryptKey(options); //returns the promise
}






/*
function myStart (pubkey) {
	var textFile = null,
  makeTextFile = function (text) {
    var data = new Blob([text], {type: 'text/plain'});

    // If we are replacing a previously generated file we need to
    // manually revoke the object URL to avoid memory leaks.
    if (textFile !== null) {
      window.URL.revokeObjectURL(textFile);
    }

    textFile = window.URL.createObjectURL(data);

    // returns a URL you can use as a href
    return textFile;
  };
  return makeTextFile("eee");
	//sw = new StreamWriter("TestFile.txt");
	//sw.Write(pubkey);
}*/



function encryptMessage(recipient_email_address, sender_email_address, passphrase, message){
	/*this function encrypts a message.
	Input: (string) recipient_email_address (address receiving), (string) sender_email_address, 
	(string) passphrase (used to decrypt senders private key to sign), (string) message (the text to encrypt)(
	Output: (string) encryptedText (the message encrypted with the recipients public key, signed
	by the senders decrypted private key)
	*/
	pubkey = getPublicKey(recipient_email_address);
	privkey = openpgp.privateKeys.getForAddress(sender_email_address);
	decryptPrivateKey(privkey[0], passphrase).then(function(key){  //decrypts private key
	var options = {
		data: message, //the message
		publicKeys: pubkey, //for encrypting
		privateKeys: key.key //for signing
	};
	openpgp.encrypt(options).then(function(ciphertext) { //encrypts email
		encryptedText = ciphertext.data; //the encrypted text
		ciphertext = undefined;
		message = undefined;
		pubkey = undefined;
		privkey = undefined;
		key = undefined;
		//not garbage collecting passphrase yet
		return encryptedText;
	}).catch(function(e){
		console.log("Caught: " + e.message);
		return {error: true, reason: e.message};
	});
	}).catch(function(e){
		console.log("Caught: " + e.message);
		return {error: true, reason: e.message};
	});
}

function encryptMessage2(){ 
/*
This function encrypts a message.
Input: (from the document) receiversEmail (for getting their public key, from textbox id="receiversEmail"), a message (from textbox id="myMessage"),
and a private key index and passphrase (from textbox id="privateKeyIndex" and textbox id="PassphraseDec") for decrypting private key for signature

Output: encryptedText, the encrypted pgp message with signature

NOTE: This function exists for testing purposes.  It will have no impact on the final product.

*/
	
	receiversEmail = document.getElementById("receiversEmail").value; //for specifying who to send to 
	
	sendersEmail = document.getElementById("sendersEmail").value;
	passphrase = document.getElementById("PassphraseDec").value;
	var myMessage = document.getElementById("myMessage").value; //the message to encrypt
	encryptedText = encryptMessage(receiversEmail, sendersEmail, passphrase, myMessage);
	/*decryptPrivateKey().then(function(key){  //decrypts private key
	var options = {
		data: myMessage, //the message
		publicKeys: pubkey, //for encrypting
		privateKeys: key.key //for signing
	};
	openpgp.encrypt(options).then(function(ciphertext) { //encrypts email
		c = ciphertext;
		encryptedText = c.data; //the encrypted text
		options = null;
		key = null;
		return encryptedText;
	});
	});*/
}


function getPublicKey(email_address){
	/*returns an array of all public keys for the given email address, or null if none
	is found */
	pubkey = openpgp.publicKeys.getForAddress(email_address);
	if(pubkey.length == 0){
		return null;
	} else return pubkey;
}

function decryptMessage(sender_email_address, receiver_email_address, encrypted_message, passphrase){
	/*
	This function decrypts a message.  
	Input: The email address of the sender of the message, the encrypted message, and the passphrase
	Output: The decrypted message, and whether the signature is valid, or not able to decrypt error
	Possible error outputs:
	If cant dectypt because wrong public key to encrypt
	throw("Key pair does not match (not yet implemented)
	*/
	
	var pubkey = getPublicKey(sender_email_address); //gets the public key of the sender to validate signature
	var privkey = openpgp.privateKeys.getForAddress(receiver_email_address);

	
	
	
	decryptPrivateKey(privkey[0], passphrase).then(function(key){ //decrypts private key
	options = { //message, decrypted private key, and public keys
		message: openpgp.message.readArmored(encrypted_message), //the encrypted message
		privateKey: key.key, //decrypted private key for decryption of message
	}; 
	openpgp.decryptSessionKey(options).then(function(sessionKey){ //uses message, decrypted private key, and public key to decrypt session key 
	options2 = {
		message: openpgp.message.readArmored(encrypted_message),     // parse armored message
		publicKeys: pubkey,    // for verification (optional)
		privateKey: key.key, // for decryption
		sessionKey: sessionKey //the session key is apparently needed for decryption
	};
	
	
	openpgp.decrypt(options2).then(function(text2) { //decrypts the message
		sessionKey = undefined;
		key = undefined;
		decrypted = text2; //everything about the decrypted message, including signature and decrypted message
		decryptedText = decrypted.data; //just the decrypted text itself,
		var isValid = decrypted.signatures[0].valid;
		var retVar = {
			decryptedText: decryptedText,
			isValid: isValid
		};
		decryptedText = undefined;
		isValid = undefined;
		options3 = undefined;
		passphrase = undefined;
		return retVar;
	}).catch(function(e){
		console.log("Caught: " + e.message);
		window.alert(e.message);
		return {error: true, reason: e.message};
	});
	}).catch(function(e){
		console.log("Caught: " + e.message);
		window.alert(e.message);
		return {error: true, reason: e.message};
	});
	}).catch(function(e){
		console.log("Caught: " + e.message);
		window.alert(e.message);
		return {error: true, reason: e.message};
	});

}

function decryptMessage2(){
	/*
	This function decrypts the encryptedText variable.  
	Input: encryptedText (atm a variable from encryptMessage(), senders email (atm it is input from a textbox, used to validate signatures),
		privKeyIndex(index of your encrypted private key in memory, input by textbox id = "privatekeyIndex"), passphrase(atm entered in by textbox id="PassphraseDec")
	Output: decryptedText (the decrypted text), and whether the signature is valid. decryptedText is a variable readable by console, but the signatures validity
		is output solely via window alert
    */
	
	sendersEmail = document.getElementById("sendersEmail").value; //reads the email of the sender
	passphrase = document.getElementById("PassphraseDec").value;
	receiversEmail = document.getElementById("receiversEmail").value;
	decryptMessage(sendersEmail, receiversEmail, encryptedText, passphrase);
	

}




document.addEventListener('DOMContentLoaded', function () {
	requirejs(['openpgp'], function(openpgp){

//openpgp.initWorker({ path:'openpgp.worker.js' });
//openpgp.initWorker({ path:'openpgpjs-master/dist/openpgp.worker.js' });
//openpgp.config.aead_protect = true;
//openpgp.initWorker({ path:'openpgpjs-master/dist/openpgp.worker.js' });
	openpgp.config.aead_protect = true;
	});
	/*var client_id = "85307012986-i22grgk3mcfs1udcapdmthri38vfbkg7.apps.googleusercontent.com";
  chrome.identity.getAuthToken(
	{'interactive': true},
	function(){
	  //load Google's javascript client libraries
		window.gapi_onload = authorize;
		loadScript('https://apis.google.com/js/client.js');
	}
);*/
  openpgp = window.openpgp;
  openpgp.Keyring(openpgp.storeHandler);
  document.getElementById('genButton').addEventListener('click', generateKeyPair2);
  document.getElementById('encryptMessageButton').addEventListener('click', encryptMessage2);
  document.getElementById('decryptPrivateKeyButton').addEventListener('click', decryptPrivateKey2);
  document.getElementById('decryptMessageButton').addEventListener('click', decryptMessage2);
  
  /*document.getElementById("decryptPrivateKeyButton").addEventListener("`click", function (){
	myDecryptionPassphrase = document.getElementById("PassphraseDec").value;
	decryptPrivateKey(privkey, pubkey, myKey, myDecryptionPassphrase).then(function(key){
		privkeyDE = key;
	});*/
});

