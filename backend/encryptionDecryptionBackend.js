//Note to self:Remember to garbage collect encryptedText eventually for final version; the var is only needed for testing
/*
Input: A name, email address, and passphrase
Output: A promise that resolves to:
{error, data}
On success, new private public key pair stored in key ring, 
promise returned resolves to {error = false, data = ""}
On failure, promise resolves to {error = true, data = error message}
*/
function generateKeyPair(name, email_address, passphrase){
	return new Promise(function(resolve, reject) {
		var options = {
	    	userIds: [{ name: name, email:email_address }], // multiple user IDs possible supposedly, not sure what that would result in
	    	numBits: 2048,        // RSA key size
	    	passphrase: passphrase         // protects the private key
			//keyExpirationTime: 90 //key expiration time, this may need to change
		};
		openpgp.generateKey(options).then(function(key){
			var privkey = key.privateKeyArmored; //armored private key
			var pubkey = key.publicKeyArmored; //armored public key
			var allPubKeys = openpgp.storeHandler.loadPublic();//gets an array of unarmored public keys
			allPubKeys.push(openpgp.key.readArmored(pubkey).keys[0]); //appends the generated public key to the list of unarmored public keys
			openpgp.storeHandler.storePublic(allPubKeys); //stores the new array of public keys to the store handler, eg: adds the key to the key ring
			var allPrivKeys = openpgp.storeHandler.loadPrivate();
			allPrivKeys.push(openpgp.key.readArmored(privkey).keys[0]);
			openpgp.storeHandler.storePrivate(allPrivKeys);
			openpgp.publicKeys.importKey(pubkey); //adds the new armored keys to openpgp.publicKeys and openpgp.privateKeys
			openpgp.privateKeys.importKey(privkey);
			
			pubkey = undefined; //garbage collection
			privkey = undefined;
			allPubKeys = undefined;
			allPrivKeys = undefined;
			resolve ({error: false, data: ""}); 
		}).catch(function(e) {
			console.log("Caught: " + e.message);
			resolve({error: true, data: e.message});
		}); 
	});
}

/*
Input: email address and public key (the public key should be armored)
Output: The key is stored in the key ring under the specified email address
*/
function storePublicKey(email_address, public_key){
	currUserId = openpgp.key.readArmored(public_key).keys[0].users[0].userId.userid;
	//the user id is in the format "name <email>"
	len = currUserId.length;
	index_of_email = currUserId.indexOf('<'); //gets the index of the start of the email block
	email_address = currUserId.substring(index_of_email + 1, len - 1);
	var pubkey = getPublicKey(email_address);
	if(pubkey != null){
		revokePublicKey(email_address);
	}
	var allPubKeys = openpgp.storeHandler.loadPublic();//gets an array of unarmored public keys
	allPubKeys.push(openpgp.key.readArmored(public_key).keys[0]); //appends the generated public key to the list of unarmored public keys
	openpgp.storeHandler.storePublic(allPubKeys); //stores the appended array to the storeHandler
	openpgp.Keyring(openpgp.storeHandler); //updates the key ring
	allPubKeys = undefined; //garbage collects
}

//read in a new public key
function readArmoredTest(email_address, public_key) {
    //var allPubKeys = openpgp.storeHandler.loadPublic();
    return openpgp.key.readArmored(public_key);
}

/*
Input: None
Output: (String) A randomly generated passphrase containing 'length' number of words
*/
function generatePassphrase() {
    var englishDictionary = getDictionary();
    var passphrase = "";
    var length = 5;
    var i;
	
    for (i = 0; i < length; i++) {
        passphrase = passphrase.concat(englishDictionary[openpgp.crypto.random.getSecureRandom(0,englishDictionary.length-1)]);
        if (i != length -1) {
            passphrase = passphrase.concat("_");
        }
    }
    
    return passphrase;
}

/*
Input: Armored private key
Output: Private key and public key matching the private key stored on key ring
*/
function getPublicKeyFromPrivate(privkey) {
	return new Promise(function(resolve, reject) {
		privKeyUnarmored = openpgp.key.readArmored(privkey).keys[0];
		pubKeyUnarmored = privKeyUnarmored.toPublic();
		pubkey = pubKeyUnarmored.armor();
		var allPubKeys = openpgp.storeHandler.loadPublic();//gets an array of unarmored public keys
		allPubKeys.push(pubKeyUnarmored); //appends the generated public key to the list of unarmored public keys
		openpgp.storeHandler.storePublic(allPubKeys); //stores the new array of public keys to the store handler, eg: adds the key to the key ring
		var allPrivKeys = openpgp.storeHandler.loadPrivate();
		allPrivKeys.push(privKeyUnarmored);
		openpgp.storeHandler.storePrivate(allPrivKeys);
		openpgp.publicKeys.importKey(pubkey); //adds the new armored keys to openpgp.publicKeys and openpgp.privateKeys
		openpgp.privateKeys.importKey(privkey);
		privkey = undefined;
		pubkey = undefined;
		privKeyUnarmored = undefined;
		pubKeyUnarmored = undefined;
		allPrivKeys = undefined;
		allPubKeys = undefined;
		resolve({error: false, data: ""});
	}).catch (function (e){
		resolve({error: true, data: e.message});	
	});
};

/*
This function decrypts the given private key using the given passphrase
*/
function decryptPrivateKey(privKey, passphrase){
	var options = {
		privateKey: privKey,
		passphrase: passphrase
	};
	privKey = undefined; //garbage collection
	passpharase = undefined; //garbage collection
	
	privkeyPromise = openpgp.decryptKey(options); //returns the promise
	options = undefined;
	return privkeyPromise;
}

// This function tests if a passphrase is correct to decrypt the passphrase
function checkPassphrase(email_address, passphrase) {
    var privKey = openpgp.privateKeys.getForAddress(email_address)[0];

    var options = {
		privateKey: privKey,
		passphrase: passphrase
    };

    privkeyPromise = openpgp.decryptKey(options);
    options = undefined;
    privKey = undefined; //garbage collection                                                                                                                                       
    passpharase = undefined; //garbage collection 

    return privkeyPromise;
}

function revokePublicKey(email_address){
	var pubKeys = openpgp.storeHandler.loadPublic(); //loads the public keys from keyring storeHandler
	var key_to_remove = openpgp.publicKeys.getForAddress(email_address); //gets the key to remove
    var index = openpgp.publicKeys.keys.indexOf(key_to_remove[0]); //gets the index of the key to remove
	if(index != -1){
		pubKeys.splice(index, 1); //removes the key from the pubKeys variable
		openpgp.storeHandler.storePublic(pubKeys); //updates the store handler
		openpgp.Keyring(openpgp.storeHandler); //updates the key ring with the updated store handler
	}
}

function revokeKeyPair(email_address){
	revokePublicKey(email_address); //removes the public key associated with this email
	
	var privKeys = openpgp.storeHandler.loadPrivate(); //loads the private keys from keyring storeHandler
	var key_to_remove = openpgp.privateKeys.getForAddress(email_address); //gets the key to remove
    var index = openpgp.privateKeys.keys.indexOf(key_to_remove[0]); //gets the index of the key to remove in openpgp.privateKeys (will be same index in storeHandler)
	if(index != -1){
		privKeys.splice(index, 1); //removes the key from the privKeys variable
		openpgp.storeHandler.storePrivate(privKeys); //updates the store handler
		openpgp.Keyring(openpgp.storeHandler); //formalizes the change, so that openpgp.privateKeys recognizes the change
	}
}

function getContactList(){
	var userIds = [];
	for(i = 0; i < openpgp.publicKeys.keys.length; i++){
		currUserId = openpgp.publicKeys.keys[i].users[0].userId.userid;
		//the user id is in the format "name <email>"
		len = currUserId.length;
		index_of_email = currUserId.indexOf('<'); //gets the index of the start of the email block
		
		name = currUserId.substring(0, index_of_email - 1);
		email_address = currUserId.substring(index_of_email + 1, len - 1);
		userIds.push({name: name, email_address: email_address});
	}
	return userIds;
}

function encryptMessage(recipient_email_address, sender_email_address, passphrase, message) {
	/*this function encrypts a message.
	Input: (string) recipient_email_address (address receiving), (string) sender_email_address, 
	(string) passphrase (used to decrypt senders private key to sign), (string) message (the text to encrypt)(
	Output: A promise that resolves to:
	An object {error (boolean), data (string)} 
	If there was an error thrown in openpgp, error will be true and data will be the error message
	If there was no error, error will be false, and data will be the message encrypted 
	with the recipients public key, signed with the senders decrypted private key
	*/
	
	return new Promise(function(resolve, reject) {
		var pubkey = getPublicKey(recipient_email_address);
		if(recipient_email_address != sender_email_address){
			pubkey.push(getPublicKey(sender_email_address)[0]);
		}
		var privkey = openpgp.privateKeys.getForAddress(sender_email_address);
		decryptPrivateKey(privkey[0], passphrase).then(function(key){  //decrypts private key
			var options = {
				data: message, //the message
				publicKeys: pubkey, //for encrypting
				privateKeys: key.key //for signing
			};
	
			openpgp.encrypt(options).then(function(ciphertext) { //encrypts email
				//encryptedText = ciphertext.data; //the encrypted text
				//ciphertext = undefined;
				message = undefined;
				pubkey = undefined;
				privkey = undefined;
				key = undefined;
				passphrase = undefined;
				encryptedText = {
					error: false, 
					data: ciphertext.data
				};
				resolve(encryptedText);
			}).catch(function(e){
				console.log("Caught: " + e.message);
				encryptedText = {error: true, data: e.message};
				resolve(encryptedText);
			});
		}).catch(function(e){
		console.log("Caught: " + e.message);
		encryptedText = {error: true, data: e.message};
		resolve(encryptedText);
	});
	});
}


function encryptMessageMulti(recipient_email_addresses, sender_email_address, passphrase, message){
	/*
	Input: Email addresses array, sender email address, passphrase, and the message
	Output: A promise that resolves to:
	An object {error (boolean), data (string)} 
	If there was an error thrown in openpgp, error will be true and data will be the error message
	If there was no error, error will be false, and data will be the message encrypted 
	with the recipients public key, signed with the senders decrypted private key
	*/
	return new Promise(function(resolve, reject){
	var pubkeys = [];
	var hasMine = false;
	for(i = 0; i < recipient_email_addresses.length; i++){
		pubkeys.push(getPublicKey(recipient_email_addresses[i])[0]);
		if(recipient_email_addresses[i] == sender_email_address){
			hasMine = true;
		}
	}
	if(hasMine == false){
		pubkeys.push(getPublicKey(sender_email_address)[0]); //adds your public key if it is not already on there
		//this way you can read your own encrypted email
	}
	var privkey = openpgp.privateKeys.getForAddress(sender_email_address);
	decryptPrivateKey(privkey[0], passphrase).then(function(key){  //decrypts private key
	var options = {
		data: message, //the message
		publicKeys: pubkeys, //for encrypting
		privateKeys: key.key //for signing
	};
	
	openpgp.encrypt(options).then(function(ciphertext) { //encrypts email
		//encryptedText = ciphertext.data; //the encrypted text
		//ciphertext = undefined;
		message = undefined;
		pubkey = undefined;
		privkey = undefined;
		key = undefined;
		passphrase = undefined;
		encryptedText = {
			error: false, 
			data: ciphertext.data
			};
		resolve(encryptedText);
	}).catch(function(e){
		console.log("Caught: " + e.message);
		encryptedText = {error: true, data: e.message};
		resolve(encryptedText);
	});
	}).catch(function(e){
		console.log("Caught: " + e.message);
		encryptedText = {error: true, data: e.message};
		resolve(encryptedText);
	});
	});
}
/*
function encryptMessageMulti(recipient_keys, sender_email_address, passphrase, message) {
	/*this function encrypts a message.
	Input: (Array) recipient_keys (list of keys for recipients), (string) sender_email_address, 
	(string) passphrase (used to decrypt senders private key to sign), (string) message (the text to encrypt)(
	Output: A promise that resolves to:
	An object {error (boolean), data (string)} 
	If there was an error thrown in openpgp, error will be true and data will be the error message
	If there was no error, error will be false, and data will be the message encrypted 
	with the recipients public key, signed with the senders decrypted private key
	*/
	/*return new Promise(function(resolve, reject) {
		var privkey = openpgp.privateKeys.getForAddress(sender_email_address);
		decryptPrivateKey(privkey[0], passphrase).then(function(key){  //decrypts private key
			var options = {
				data: message, //the message
				publicKeys: recipient_keys, //for encrypting
				privateKeys: key.key //for signing
			};
	
			openpgp.encrypt(options).then(function(ciphertext) { 
				message = undefined;
				recipient_keys = undefined;
				privkey = undefined;
				key = undefined;
				passphrase = undefined;
				encryptedText = {
					error: false, 
					data: ciphertext.data
				};
				resolve(encryptedText);
			}).catch(function(e){
				console.log("Caught: " + e.message);
				encryptedText = {error: true, data: e.message};
				resolve(encryptedText);
			});
		}).catch(function(e){
		console.log("Caught: " + e.message);
		encryptedText = {error: true, data: e.message};
		resolve(encryptedText);
	});
	});
}*/

function getPublicKey(email_address) {
	/*returns an array of all public keys for the given email address, or null if none
	is found */
	var pubkey = openpgp.publicKeys.getForAddress(email_address);
	if(pubkey.length == 0){
		return null;
	} else return pubkey;
}

// Takes in a list of email addresses and returns either true
function getPublicKeyList(recipient_emails) {
    missing_keys = false;
    return_list = [];
    for(i = 0; i < recipient_emails.length; i++){
        recipient_email_address = recipient_emails[i];
        next_key = getPublicKey(recipient_email_address);
        if (!missing_keys) {
            if (next_key) {
                return_list.push(next_key[0]);
            } else {
                missing_keys = true;
                return_list = [recipient_email_address];
            }
        } else {
            if (!next_key) {
               return_list.push(recipient_email_address);
            }
        }
    }
    return {error: missing_keys, data: return_list};
}

function getArmoredPublicKey(email_address) {
    var pubkey = openpgp.publicKeys.getForAddress(email_address);
    if(pubkey.length == 0){
	return null;
    } else return pubkey[0].armor();

}

function getPrivateKey(email_address){
	/*returns an array of all private keys for the given email, or null if none 
	is found */
	var privkey = openpgp.privateKeys.getForAddress(email_address);
	if(privkey.length == 0){
		return null;
	} else return privkey;
}			    

function decryptMessage(sender_email_address, receiver_email_address, encrypted_message, passphrase) {
	/*
	This function decrypts a message.  
	Input: The email address of the sender of the message, the encrypted message, and the passphrase
	Output: Promise that resolves to object:
	{error (boolean), data: (string/object)}
	If success, error = false, data = {decryptedText (string), isValidSig (boolean)}
	If failure, error = true, data = error message from openpgp
	*/
	
	var pubkey = getPublicKey(sender_email_address); //gets the public key of the sender to validate signature
	var privkey = openpgp.privateKeys.getForAddress(receiver_email_address); //gets the private key of the receiver to decrypt

	
	
	return new Promise (function(resolve, reject) {
		decryptPrivateKey(privkey[0], passphrase).then(function(key){ //decrypts private key using passphrase
			var options = { //message, decrypted private key, and public keys
				message: openpgp.message.readArmored(encrypted_message), //the encrypted message
				privateKey: key.key, //decrypted private key for decryption of message
			}; 
			openpgp.decryptSessionKey(options).then(function(sessionKey){ //uses message, decrypted private key, and public key to decrypt session key 
				var options2 = {
					message: openpgp.message.readArmored(encrypted_message),     // parse armored message
					publicKeys: pubkey,    // for verification (optional)
					privateKey: key.key, // for decryption
					sessionKey: sessionKey //the session key is apparently needed for decryption
				};
		
		
				openpgp.decrypt(options2).then(function(text2) { //decrypts the message
					sessionKey = undefined;
					key = undefined;
					decrypted = text2; //everything about the decrypted message, including signature and decrypted message
					decryptedText = decrypted.data; //just the decrypted text itself,
					var isValid = decrypted.signatures[0].valid;
					var info = {
						decryptedText: decryptedText,
						isValidSig: isValid
					};
			
					decryptedText = undefined; //garbage collection
					isValid = undefined; 
					options3 = undefined;
					passphrase = undefined;
					decryptedRetVar = {error: false, data: info};
					resolve ({error: false, data: info});
				}).catch(function(e){
					console.log("Caught: " + e.message);
					decryptedRetVar = {error: true, data: e.message};
					resolve ({error: true, data: e.message});
				});
			}).catch(function(e){
				decryptedRetVar = {error: true, data: e.message};
				console.log("Caught: " + e.message);
				resolve({error: true, data: e.message});
			});
		}).catch(function(e){
			decryptedRetVar = {error: true, data: e.message};
			console.log("Caught: " + e.message);
			resolve({error: true, data: e.message});
		});
	});
}